part of './app_pages.dart';

abstract class Routes {
  static const String splashPage = '/splashPage';
  static const String loginPage = '/loginPage';
  static const String homePage = '/homePage';
  static const String addressPage = '/addressPage';
}
