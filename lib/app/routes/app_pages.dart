
import 'package:get/get.dart';
import 'package:now_test/app/bindings/home_binding.dart';
import 'package:now_test/app/ui/pages/home/address/all_address_page.dart';
import 'package:now_test/app/ui/pages/home/home_page.dart';
import 'package:now_test/app/ui/pages/splash_page/splash_page.dart';
part 'app_routes.dart';

class AppPages {
  static final List<GetPage> routes = <GetPage>[
    GetPage(name: Routes.splashPage, page: () => const SplashPage()),
    GetPage(name: Routes.homePage, page: () => const HomePage(), binding: HomeBinding()),
    GetPage(name: Routes.addressPage, page: () => const AllAddressPage(), binding: HomeBinding()),
  ];
}
