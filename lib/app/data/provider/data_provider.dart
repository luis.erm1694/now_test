
import 'package:get/get.dart';
import 'package:now_test/app/data/model/postal_code_model.dart';
import 'package:now_test/app/data/provider/paths_provider.dart';

class DataProvider extends GetConnect {

  PathProvider pathProvider = PathProvider();

  Future<PostalCodeResponseModel> searchByCp({required String postalCode}) async {
    String url = '${PathProvider.baseUrl}${PathProvider.postalCodePath}$postalCode';
    var response = await get(url);
    return PostalCodeResponseModel.fromJson(response.body);
  }

}