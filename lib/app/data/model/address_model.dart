class AddressModel {
  AddressModel(
   { 
    required this.street, 
    required this.externalNumber, 
    required this.postalCode, 
    required this.state, 
    required this.municipality, 
    required this.colony, 
    required this.references, 
    required this.isDefault,
    required this.name
   }
  );

  final String name;
  final String street;
  final int externalNumber;
  final String postalCode;
  final String state;
  final String municipality;
  final String colony;
  final String references;
  final int isDefault;

  
  AddressModel copyWith({
  final String? name,
  final String? street,
  final int? externalNumber,
  final String? postalCode,
  final String? state,
  final String? municipality,
  final String? colony,
  final String? references,
  final int? isDefault

  }){
    return AddressModel(
      street: street ?? this.street, 
      externalNumber: externalNumber ?? this.externalNumber,
      postalCode: postalCode ?? this.postalCode, 
      state: state ?? this.state, 
      municipality: municipality ?? this.municipality, 
      colony: colony ?? this.colony, 
      references: references ?? this.references, 
      isDefault: isDefault ?? this.isDefault, 
      name: name ?? this.name);
  }

}