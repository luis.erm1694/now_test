import 'package:get/get.dart';
import 'package:now_test/app/data/provider/data_provider.dart';
import 'package:now_test/app/routes/app_pages.dart';

class SplashController extends GetxController {
  /* -------------------------------------------------------------------------- */
  /*                                 CONTROLLERS                                */
  /* -------------------------------------------------------------------------- */

  /* -------------------------------------------------------------------------- */
  /*                                  VARIABLES                                 */
  /* -------------------------------------------------------------------------- */

  /* -------------------------------------------------------------------------- */
  /*                                 LIFECYCLES                                 */
  /* -------------------------------------------------------------------------- */

  @override
  void onReady() {
     Get.lazyPut(() => DataProvider());
    super.onReady();
    _toNextPage();
  }

  /* -------------------------------------------------------------------------- */
  /*                                   METHODS                                  */
  /* -------------------------------------------------------------------------- */

  /// Despues de 2 segundoa envia al usuario a la pantalla de
  /// bienvenida
  void _toNextPage() async {
    await 2.delay();
    Get.offNamed(Routes.homePage);
  }
}
