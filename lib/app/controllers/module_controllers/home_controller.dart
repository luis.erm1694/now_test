import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:now_test/app/data/model/address_model.dart';
import 'package:now_test/app/data/model/postal_code_model.dart';
import 'package:now_test/app/data/provider/data_provider.dart';
import 'package:now_test/app/ui/widgets/custom_flushbar.dart';
import 'package:now_test/app/utils/common_validator.dart';

class AddressController extends GetxController {
  /* -------------------------------------------------------------------------- */
  /*                                 CONTROLLERS                                */
  /* -------------------------------------------------------------------------- */

  TextEditingController streetController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController externalNumberController = TextEditingController();
  TextEditingController postalCodeController = TextEditingController();
  TextEditingController referencesController = TextEditingController();
  TextEditingController municipalityController = TextEditingController();
  TextEditingController colonyController = TextEditingController();
  TextEditingController stateController = TextEditingController();

  /* -------------------------------------------------------------------------- */
  /*                                  VARIABLES                                 */
  /* -------------------------------------------------------------------------- */

  late final DataProvider _provider;
  RxBool isDefault = false.obs;
  final validator = CommonValidator();
  RxBool isLoading = false.obs;
  late  Rx<PostalCodeResponseModel>? responseCp = PostalCodeResponseModel().obs;
  RxInt selectedIndexDropdown = (-1).obs;
  final formKey = GlobalKey<FormState>();
  RxList<AddressModel> myAddress = RxList();
  RxBool isEditing = false.obs;
  late AddressModel addressSelected;
  int indexAddres = -1;


  /* -------------------------------------------------------------------------- */
  /*                                 LIFECYCLES                                 */
  /* -------------------------------------------------------------------------- */

  @override
  void onInit() {
    _provider = Get.find<DataProvider>();
    super.onInit();
  }
  


  /* -------------------------------------------------------------------------- */
  /*                                   METHODS                                  */
  /* -------------------------------------------------------------------------- */

  String? isValidDropdown(int? value) {
    return (value != null && value != -1) ? null : 'Este campo es requerido';
  }
  void colonyChanged({required int? value}) {
    
    selectedIndexDropdown.value = value!;
    colonyController.text = responseCp!.value.sepomex![value].asentamiento ?? 'N/A';
    stateController.text = responseCp!.value.sepomex![value].estado ?? 'N/A';
    municipalityController.text = responseCp!.value.sepomex![value].municipio ?? 'N/A';
  }

  Future searchByCp({required String postalCode})async {
    isLoading.value = true;
    try {
      selectedIndexDropdown.value = -1;
      responseCp!.value = await _provider.searchByCp(postalCode: postalCode);
      if(responseCp!.value.sepomex!.isNotEmpty){
        stateController.text = responseCp!.value.sepomex![0].estado ?? 'N/A';
        municipalityController.text = responseCp!.value.sepomex![0].municipio ?? 'N/A';
      }
    } catch (e) {
      CustomFlushbar(message: 'No tenemos cobertura en esta zona\nPuedes intentar con otro código postal').flushbar.show(Get.context!);
      stateController.clear();
      colonyController.clear();
      municipalityController.clear();
    }
    isLoading.value = false;
  }

  void postalCodeChanged({required String postalCode}) {
    if (postalCode.length == 5) {
      searchByCp(postalCode: postalCode);
      
    }
  }

  addAddress(){
    if (formKey.currentState!.validate() && selectedIndexDropdown.value != -1) {
      AddressModel newAddress = AddressModel(
        street: streetController.text, 
         externalNumber: int.tryParse(externalNumberController.text)!, 
         postalCode: postalCodeController.text, 
         state: stateController.text, 
         municipality: municipalityController.text, 
         colony: colonyController.text, 
         references: referencesController.text, 
         isDefault:  isDefault.value? 1 : 0,
         name: nameController.text
      );
      myAddress.add(newAddress);
      isDefault.value = false;
      CustomFlushbar(message: 'Se ha agregado ${nameController.text} correctamente').flushbar.show(Get.context!);
      clearFields();
    }
  }

  clearFields(){
    responseCp?.value = PostalCodeResponseModel(sepomex: null);
    selectedIndexDropdown.value = -1;
    nameController.clear();
    referencesController.clear();
    streetController.clear();
    externalNumberController.clear();
    postalCodeController.clear();
    municipalityController.clear();
    colonyController.clear();
    stateController.clear();
    isDefault.value = false;
  }

  deleteAddress({required int index}){
    myAddress.removeAt(index);
    update();
  }

  setAddressFields(){
    nameController.text = addressSelected.name;
    referencesController.text = addressSelected.references;
    streetController.text = addressSelected.street;
    externalNumberController.text = addressSelected.externalNumber.toString();
    postalCodeController.text = addressSelected.postalCode;
    municipalityController.text = addressSelected.municipality;
    colonyController.text = addressSelected.colony;
    stateController.text = addressSelected.state;
    addressSelected.isDefault == 1? isDefault.value = true : false;
    Get.back();

    update();

  }

  editAddress(){
    if (formKey.currentState!.validate()) {
      myAddress[indexAddres] = AddressModel(
        street: streetController.text, 
         externalNumber: int.tryParse(externalNumberController.text)!, 
         postalCode: postalCodeController.text, 
         state: stateController.text, 
         municipality: municipalityController.text, 
         colony: colonyController.text, 
         references: referencesController.text, 
         isDefault:  isDefault.value? 1 : 0,
         name: nameController.text
      );
      CustomFlushbar(message: 'Se editó ${nameController.text} correctamente').flushbar.show(Get.context!);
      isEditing.value = false;
      clearFields();
    }
  }

}
