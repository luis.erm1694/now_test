import 'package:get/get.dart';
import 'package:now_test/app/controllers/module_controllers/splash_controller.dart';

class SplashBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<SplashController>(SplashController());
  }
}
