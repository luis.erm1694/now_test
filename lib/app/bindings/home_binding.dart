import 'package:get/get.dart';
import 'package:now_test/app/controllers/module_controllers/home_controller.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.put<AddressController>(AddressController());
  }
}
