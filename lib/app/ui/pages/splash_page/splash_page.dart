import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:now_test/app/controllers/module_controllers/splash_controller.dart';
import 'package:now_test/app/ui/theme/colors.dart';
import 'package:now_test/app/utils/assets_path.dart';

class SplashPage extends GetView<SplashController> {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.dark,
      body: SafeArea(
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(64.0),
            child: Image.asset(AssetsPath.imgLogo),
          ),
        ),
      ),
    );
  }
}
