// ignore_for_file: invalid_use_of_protected_member

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:now_test/app/controllers/module_controllers/home_controller.dart';
import 'package:now_test/app/data/model/address_model.dart';
import 'package:now_test/app/ui/theme/colors.dart';

class AddressWidget extends GetView<AddressController> {
  const AddressWidget( {super.key, required this.address, required this.index});
  final AddressModel address;
  final int index;
  

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top:15),
      width: Get.width,
      decoration:  BoxDecoration(
        borderRadius: BorderRadius.circular(7),
        color: Colors.white,
      ),
      padding: const EdgeInsets.all(15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              const Icon(Icons.home_work_rounded, 
                color: CustomColors.btnPrimary,
              ),
              const SizedBox(width: 20,),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(address.name, 
                    style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                  Text(address.street,
                    style: const TextStyle(fontWeight: FontWeight.w500, fontSize: 16),
                  ),
                ],
              ),
            ],
          ),
          Row(
            children: [
              Container(
                padding: const EdgeInsets.all(7),
                decoration: const BoxDecoration(
                  color: CustomColors.btnPrimary,
                  shape: BoxShape.circle
                ),
                child: InkWell(
                  onTap:() {
                    controller.addressSelected = address;
                    controller.indexAddres = index;
                    controller.isEditing.value = true;
                    controller.setAddressFields();
                    Get.back();
                  },
                  child: const Icon(Icons.edit, color: Colors.white,),
                ),
              ),
              const SizedBox(width: 15,),
              Container(
                padding: const EdgeInsets.all(7),
                decoration: const BoxDecoration(
                  color: Color.fromARGB(255, 245, 107, 107),
                  shape: BoxShape.circle
                ),
                child: InkWell(
                  onTap:() => controller.deleteAddress(index: index),
                  child: const Icon(Icons.clear_rounded),
                ),
              ),
              
              
            ],
          ),
          
        ],
      )
    );
  }
}
