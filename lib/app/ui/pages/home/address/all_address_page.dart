// ignore_for_file: invalid_use_of_protected_member

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:now_test/app/controllers/module_controllers/home_controller.dart';
import 'package:now_test/app/ui/pages/home/widget/addres_widget.dart';
import 'package:now_test/app/ui/theme/colors.dart';

class AllAddressPage extends GetView<AddressController> {
  const AllAddressPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: CustomColors.primaryColor,
        title: const Text(
          'Mis Direcciones',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w900
          ),
        ),
      ),
      body: Container(
        width: Get.width,
        decoration: const BoxDecoration(
          color: CustomColors.primaryColor,
        ),
        padding: const EdgeInsets.all(20),
        height: Get.height,
        child: Obx(
          () => controller.myAddress.value.isEmpty
          ? const Center(
            child: Text('Aqui podrás ver las direcciones que has agregado',
              style: TextStyle(color: Colors.white, fontWeight: FontWeight.w900, fontSize: 18),
              textAlign: TextAlign.center,
            
            )
          )
          : Obx(() => ListView.builder(
                shrinkWrap: true,
                itemCount: controller.myAddress.value.length,
                itemBuilder: (BuildContext context, int index) {
                  return AddressWidget(
                    address: controller.myAddress.value[index],
                    index: index,
                  );
                },
              )),
        ),
      ),
    );
  }
}
