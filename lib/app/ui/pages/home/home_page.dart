import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:now_test/app/controllers/module_controllers/home_controller.dart';
import 'package:now_test/app/routes/app_pages.dart';
import 'package:now_test/app/ui/theme/colors.dart';
import 'package:now_test/app/ui/widgets/custom_textfield.dart';

class HomePage extends GetView<AddressController> {
  const HomePage({super.key});
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: CustomColors.primaryColor,
        title:const Text('Agrega una dirección', 
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Container(
        decoration: const BoxDecoration(
          color: CustomColors.primaryColor,
        ),
        padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
        height: Get.height,
        child: SingleChildScrollView(
          child: Obx(() => Stack(
            alignment: Alignment.center,
            children: [
              Form(
                key: controller.formKey,
                child: Column(
                  children: [
                     CustomTextField(
                      controller: controller.nameController,
                      validator: controller.validator.isRequired,
                      title: 'Nombre',
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(128),
                      ],
                    ),
                     CustomTextField(
                      validator: controller.validator.isRequired,
                      controller: controller.streetController,
                      title: 'Calle',
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(128),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children:[
                        CustomTextField(
                          validator: controller.validator.isRequired,
                          controller: controller.postalCodeController,
                          width: Get.width*.42,
                          title: 'C.P',
                          keyboardType: TextInputType.phone,
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly,
                            LengthLimitingTextInputFormatter(5),
                          ],
                          onChanged: (value) => controller.postalCodeChanged(postalCode: value),
                        ),
                        CustomTextField(
                          controller: controller.externalNumberController,
                          width: Get.width*.42,
                          keyboardType: TextInputType.phone,
                          validator: controller.validator.isRequired,
                          title: 'Número exterior',
                        )
                      ],
                    ),
                    CustomTextField(
                      readOnly: true,
                      controller: controller.municipalityController,
                      validator: controller.validator.isRequired,
                      title: 'Municipio',
                    ),
                    CustomTextField(
                      readOnly: true,
                      controller: controller.stateController,
                      validator: controller.validator.isRequired,
                      title: 'Estado',
                    ),
                    controller.responseCp?.value.sepomex == null?
                    CustomTextField(
                      readOnly: true,
                      controller: controller.colonyController,
                      validator: controller.validator.isRequired,
                      title: 'Asentamiento',
                    )
                    : Container(
                      margin: const EdgeInsets.only(top:30),
                      width: Get.width,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5)
                      ),
                      child: Center(
                        child: DropdownButtonFormField<int>(
                          validator: controller.validator.isValidDropdown,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          isExpanded: true,
                          focusColor: Colors.white,
                          value: controller.selectedIndexDropdown.value == -1 ? null : controller.selectedIndexDropdown.value,
                          iconEnabledColor: Colors.black,
                          hint: const Padding(
                            padding:  EdgeInsets.only(left:5.0),
                            child:  Text('Asentamiento'),
                          ),
                          items:  [
                            for(int i=0; i<controller.responseCp!.value.sepomex!.length; i++) ...[
                              DropdownMenuItem<int>(
                                value: i,
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 5.0),
                                  child: Text(
                                    controller.responseCp?.value.sepomex![i].asentamiento ?? 'N/A',
                                    style: const TextStyle(color: CustomColors.btnPrimary, fontWeight: FontWeight.w900, fontSize: 16),
                                  ),
                                ),
                              ),
                            ],
                          ],
                           onChanged: (value) => controller.colonyChanged(value: value),
                        ),
                      ),
                    ),
                    CustomTextField(
                      controller: controller.referencesController,
                      validator: controller.validator.isRequired,
                      title: 'Referencias',
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(256),
                      ],
                    ),
                    const SizedBox(height: 20,),
                    Row(
                      children: [
                        CupertinoSwitch(
                          value: controller.isDefault.value,
                          onChanged: (bool newValue) {
                            controller.isDefault.value = newValue;
                          },
                          activeColor: CustomColors.btnPrimary,
                          trackColor: CupertinoColors.systemGrey5,
                        ),
                        const SizedBox(width: 15,),
                        const Expanded(
                          child: Text(
                            'Seleccionar esta dirección como predeterminada', 
                            softWrap: true,
                            style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    ),
                    const SizedBox(height: 20,),
                    MaterialButton(
                      child: Container(
                        width: Get.width*.45,
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: CustomColors.btnPrimary
                        ),
                        child: Center(
                          child: Text(
                            controller.isEditing.value? 'Editar' :'Agregar',
                            style: const TextStyle(color: Colors.white),
                          ),
                        )
                      ),
                      onPressed: (){
                        controller.isEditing.value? controller.editAddress() : controller.addAddress();
                      }
                    ),
                    MaterialButton(
                      child: Container(
                        width: Get.width*.45,
                        padding: const EdgeInsets.symmetric(vertical: 10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: CustomColors.btnPrimary
                        ),
                        child: const Center(
                          child: Text(
                            'Ver mis direcciones',
                            style:  TextStyle(color: Colors.white),
                          ),
                        )
                      ),
                      onPressed: (){
                        Get.toNamed(Routes.addressPage);
                      }
                    )
                  ],
                ),
              ),
              Visibility(
                visible: controller.isLoading.value,
                child: const Center(
                  child: CircularProgressIndicator(),
                ),
              )
            ],
          )
          ),
        ),
      ),
    );
  }
}
