class CustomStyles {
  /// Create a Common styles class
  ///

  /// It is used to set the radius in most of the Widgets,
  /// that need to have a border.
  static const double mainRadius = 14.0;

  /// Set the radius to main Button Widgets
  static const double mainButtonRadius = 30.0;

  /// Set the radius to normal Button Widgets
  static const double normalButtonRadius = 8.0;

  /// Set the height to main Button Widgets
  static const double mainButtonHeight = 45.0;

  /// Set the margin to main Screens or Widgets.
  static const double mainMargin = 16.0;

  /// Set the margin to Login Screen.
  static const double loginMargin = 8.0;

  /// Set the padding to Login Screen.
  static const double loginPadding = 26.0;

  /// Set the font size in most of the cases we need a big title
  static const double xxLargeFontSize = 22.0;

  /// Set the font size in most of the cases we need a title
  static const double xLargeFontSize = 20.0;

  /// Set the font size in most of the cases we need a big subtitle
  static const double largeFontSize = 18.0;

  /// Set the font size in most of the cases we need a subtitle
  static const double mediumFontSize = 16.0;

  /// Set the font size in most of the cases we need a regular text
  static const double regularFontSize = 14.0;

  /// Set the font size for text with small size
  static const double smallFontSize = 12.0;

  /// Set the font size for text with very small size
  static const double xSmallFontSize = 10.0;

  /// Altura del ConvexAppBar en tab_page.dart
  static const double convexAppBarHeight = 65.0;
}
