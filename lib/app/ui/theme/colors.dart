import 'package:flutter/material.dart';

class CustomColors {

  static const Color primaryColor = Color.fromRGBO(151, 187, 173, 1);

  /// It is used as a dark color in diferents Widgets
  static const Color dark = Color(0xFF1E2329);

  /// It is used as contrast in Widgets with dark colors
  static const Color light = Color(0xFFFFFFFF);

  /// It is used as contrast in Widgets with dark colors
  static const Color lightGrey = Color(0xFFF9FAFC);

  /// Light green color
  static const Color lightGreen = Color(0xFFd7edde);

  /// Light orange color
  static const Color lightOrange = Color(0xFFfee1cf);

  /// It is used as contrast in Widgets with ligth colors
  static const MaterialColor grey = Colors.grey;

  /// It is used as a primary color in main text
  static const Color txtPrimary = Color(0xFF606060);

  /// It is used as a primary color in buttons
  static const Color btnPrimary = Color.fromRGBO(46, 61, 64, 1);

  /// It is used as black color in buttons
  static const Color btnDark = Color(0xFF000000);

  /// It is used as blue color in flushbar messages
  static const Color messageBlue = Color(0xFF30b5e9);

  /// It is used to set the primarySwatch MaterialColor in `themes.dart` file
  static const MaterialColor darkMaterialColor = MaterialColor(0xFF1E2329, _darkMaterialColor);
  static const Map<int, Color> _darkMaterialColor = {
    50: Color.fromRGBO(30, 35, 41, .1),
    100: Color.fromRGBO(30, 35, 41, .2),
    200: Color.fromRGBO(30, 35, 41, .3),
    300: Color.fromRGBO(30, 35, 41, .4),
    400: Color.fromRGBO(30, 35, 41, .5),
    500: Color.fromRGBO(30, 35, 41, .6),
    600: Color.fromRGBO(30, 35, 41, .7),
    700: Color.fromRGBO(30, 35, 41, .8),
    800: Color.fromRGBO(30, 35, 41, .9),
    900: Color.fromRGBO(30, 35, 41, 1),
  };
}
