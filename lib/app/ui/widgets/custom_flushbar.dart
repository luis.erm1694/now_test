import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:now_test/app/ui/theme/colors.dart';

class CustomFlushbar<T> {
  /// Create a Common FLushbar widget
  ///

  CustomFlushbar({
    required String message,
    String? title,
    Color backgroundColor = CustomColors.messageBlue,
    bool isDismissible = true,
    Duration duration = const Duration(seconds: 6),
    FlushbarPosition position = FlushbarPosition.TOP,
    EdgeInsets margin = const EdgeInsets.all(8),
    BorderRadius? borderRadius,
    Curve reverseAnimationCurve = Curves.decelerate,
    Curve forwardAnimationCurve = Curves.elasticOut,
    Widget? mainButton,
  }) {
    _flushbar = Flushbar<T>(
      margin: margin,
      borderRadius: borderRadius ?? BorderRadius.circular(8),
      title: title,
      message: message,
      flushbarPosition: position,
      flushbarStyle: FlushbarStyle.FLOATING,
      reverseAnimationCurve: reverseAnimationCurve,
      forwardAnimationCurve: forwardAnimationCurve,
      backgroundColor: backgroundColor,
      isDismissible: isDismissible,
      duration: duration,
      mainButton: mainButton,
    );
  }

  late Flushbar<T> _flushbar;

  Flushbar<T> get flushbar => _flushbar;
}
