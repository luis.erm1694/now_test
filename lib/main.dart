import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:now_test/app/bindings/splash_binding.dart';
import 'package:now_test/app/routes/app_pages.dart';
import 'package:now_test/app/ui/theme/colors.dart';
import 'package:now_test/app/ui/theme/themes.dart';

void main()async {
  WidgetsFlutterBinding.ensureInitialized();
    
    // Inicializa el local Storage
    //await GetStorage.init();

    // Cambia el color del statusBar en IOS y Android a un color personalizado
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: CustomColors.dark,
      statusBarIconBrightness: Brightness.light,
    ));

    /// Inicializa el supercontrolador para manejar el ciclo de vida de la app
    //Get.put(LifeCycleAppController());

    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_) {
      runApp(
        GetMaterialApp(
          routingCallback: (routing) {
            //reportRoutingChanges(routing);
          },
          builder: BotToastInit(),
          navigatorObservers: [BotToastNavigatorObserver()],
          debugShowCheckedModeBanner: false,
          initialRoute: Routes.splashPage,
          theme: CustomTheme.defaultTheme,
          defaultTransition: Transition.fade,
          initialBinding: SplashBinding(),
          getPages: AppPages.routes,
          locale: const Locale('es', 'MX'),
        ),
      );
    });
}
